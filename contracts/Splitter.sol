pragma solidity ^0.4.6;

contract Splitter {
    
    address public owner;
    address public firstSplitter;
    address public secondSplitter;
    
    mapping (address => uint) public balanceOf;
    
    function Splitter(address firstUser, address secondUser) public {
        require(msg.sender != firstUser);
        require(msg.sender != secondUser);
        require(firstUser != secondUser);
        require(firstUser != address(0));
        require(secondUser != address(0));

        owner = msg.sender;
        firstSplitter = firstUser;
        secondSplitter = secondUser;
    }
    
    function getContractBalance() public constant returns (uint balance) {
        return this.balance;
    }
    
    function () public payable {
        require(msg.sender == owner);
        require(msg.value > 0);
        
        balanceOf[firstSplitter] += msg.value / 2;
        balanceOf[secondSplitter] += msg.value / 2;
        balanceOf[owner] += msg.value % 2;
    }
    
    function withdraw (uint withdrawAmount) public {
        require(balanceOf[msg.sender] >= withdrawAmount);
        
        balanceOf[msg.sender] -= withdrawAmount;
        msg.sender.transfer(withdrawAmount);
    }
    
    function killContract() public returns(bool success) {
        require(msg.sender == owner);
        selfdestruct(owner);
        
        return true;
    }
}